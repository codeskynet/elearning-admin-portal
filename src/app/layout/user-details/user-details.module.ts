import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {UserDetailsRoutingModule} from './user-details-routing.module';
import { UserDetailsComponent } from './user-details.component';
import { PageHeaderModule } from './../../shared';
import {ModalComponent} from './components';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [UserDetailsComponent, ModalComponent],
  imports: [
    CommonModule, UserDetailsRoutingModule, PageHeaderModule, NgbModule, AgGridModule.withComponents([])
  ]
})
export class UserDetailsModule { }
