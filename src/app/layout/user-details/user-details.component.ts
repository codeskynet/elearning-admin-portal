import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  columnDefs = [
    { headerName: 'Name', field: 'name' },
    { headerName: 'Role', field: 'role' },
    { headerName: 'Country', field: 'country' },
    { headerName: 'Active', field: 'active' },
    { headerName: 'Action', field: 'action' }
  ];

  rowData = [
    { name: 'Mark', role: 'Author', country: 'India', active: 'yes', action: 'Edit | Delete' },
    { name: 'Mark', role: 'Author', country: 'India', active: 'yes', action: 'Edit | Delete' },
    { name: 'Mark', role: 'Author', country: 'India', active: 'yes', action: 'Edit | Delete' }
  ];

  constructor() {}

  ngOnInit() {}
}
