import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';
import { LoginService } from './login.service';
import  {LoginModel} from './login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
  public loginError: Boolean = false;
  public username: String = '';
  loginModel: LoginModel = new LoginModel('', '');

  constructor(private translate: TranslateService, private _loginService: LoginService, public router: Router) {
    this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    this.username = 'hi';
  }


  ngOnInit() {}

  onLoggedin() {
    this._loginService.postAuth(this.loginModel).subscribe(
      data => {
        this.loginError = true;
        console.log(data);
        if (data[0].auth === true) {
          this.router.navigate(['/dashboard']);
        } else {
          this.loginError = true;
        }
      },
      err => console.error(err),
      () => console.log('done loading foods')
    );

    localStorage.setItem('isLoggedin', 'true');
  }
}
