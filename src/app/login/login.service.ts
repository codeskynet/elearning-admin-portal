
import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, of} from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class LoginService {

    constructor(private http: HttpClient) {}
    getAuth(): Observable<any> {
        return this.http.get('/api/login');
    }
    postAuth(postData): Observable<any> {
        return this.http.post('/api/login', {
            username: postData.username,
            password: postData.password
        });
    }
}
